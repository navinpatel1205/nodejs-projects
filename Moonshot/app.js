const express= require('express');

const app= express();
app.use(express.json());

function roothandler(req,res)
{
    res.send("hello from express");
    console.error(new Error("This is a error"))
    // console.log(req.headers,req.params,req.body,req.query)
}

function rootposthandler(req,res)
{
    res.send({Name:'NAvin'});
       console.log(req.headers,req.body)
}
app.get('/',roothandler);
app.post('/',rootposthandler);

app.listen(5000 , ()=>{
    console.log("app is listen port 5000");
})