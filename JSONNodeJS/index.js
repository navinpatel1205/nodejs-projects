
const fs = require("fs");

const data={
    name:"Navin",
    age:23,
    surname:"Patel"

};
// console.log(data);
// //object to json convert
// const jsondata=JSON.stringify(data);
// console.log(jsondata);

// //json to object convert
// const obdata=JSON.parse(jsondata);
// console.log(obdata);
// 1: conver to json
// 2: add in another File
// 3: readFile
// 4: convert back from json to object
// 5: show on console
const jsondata=JSON.stringify(data);
// fs.writeFile("json1.json",jsondata,()=>{
//     console.log("done");
// })
fs.readFile('json1.json','utf-8',(err,data)=>{
    console.log(data);
    const orgdata=JSON.parse(jsondata);
    console.log(orgdata);
})