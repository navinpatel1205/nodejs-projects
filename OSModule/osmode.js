const os= require("os");
//to see sysyem architecture
console.log(os.arch());
// to see sysyem free memory
const m=os.freemem();
console.log(` Free Memory:${m/1024/1024/1024}`);
// to see sysyem total memory
const tm=os.totalmem();
console.log(` Total Memory:${tm/1024/1024/1024}`);
//to see hostname
console.log(os.hostname());
console.log(os.platform());
console.log(os.tmpdir());
console.log(os.release());
console.log(os.type());
console.log(os.));