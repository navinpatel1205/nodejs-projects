const express= require('express');

const app=express();

app.use( express.json());
function pathprinter(req,res,next){
    console.log(req.originalUrl);
    req.name=req.body.name;
    next();
}

function authentic(req,res,next){
    const auth=req.query.authentic;
    if(auth==="1")
    {
        next();
    }
    else{
        res.status(401).send("Login Error");
    }
}

// app.use(pathprinter);

function rootGetHandler(req,res)
{  
     res.send("Hello From rootGetHandler \n Login Successfully");
    setTimeout(()=>{ 
        res.status(200).send(req.body);
    },3000);

}

function rootPostHandler(req,res)
{    console.log(req.name);
    res.send("Hello From rootPostHandler");
}

app.get("/user",authentic,rootGetHandler);
app.post("/",pathprinter,rootPostHandler)
app.listen(3000,()=>{
    console.log("app listen at port 3000");
})