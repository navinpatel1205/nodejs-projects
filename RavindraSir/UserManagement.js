const express = require('express');

const app= express();
app.use(express.json());

function urlprint(req,res,next){
    console.log(new Date().toISOString()+" :"+req.originalUrl);
    next();
}
 
app.use(urlprint);
const users=[]; 


/// function for create new user
function createuser(req,res)
{
    
    //  const {id,name,age} =req.body;
    const id=req.body.id;
    const name=req.body.Name;
    const age=req.body.age;
    // const userObject={id:id,
    //     name: name,
    //     age:age
    // }
          const userObject ={id, name, age};
          users.push(userObject);
          res.send({
              msg:"New user created",
              data:userObject 
          });
}


function getusers(req,res){
    res.send(users);
}



//function for update the user data
function UpdateData(req,res)
{
    const user_id=parseInt(req.params.user_id);
    const name=req.body.Name;
    const age=req.body.age;
   
    const userIdx =users.findIndex(elm =>elm.id == user_id );
   
    if(userIdx>=0)
{
    const Updated_user={
        id:user_id,
        name,
        age
    }
    users[userIdx]=Updated_user;
    res.send({ 
        msg:"User Update SuccesFull!",
        data:Updated_user
    })
} else{
    res.status(404).send("UserId is not correct");
}  
}


//function for delete  user data
function delete_User(req,res){
    const user_id=parseInt(req.params.user_id);
    const userIdx =users.findIndex(elm =>elm.id == user_id );
    if(userIdx>=0)
    {
            users.splice(userIdx,1);
            res.send("User Deleted");
    }else{
        res.send("User Not Found");
    }
}

//function for get user data info

function getuser(req,res){
    const user_id=parseInt(req.params.user_id);
    const user=users.find(elm =>elm.id == user_id);
     
    
    if(user)
    {
        res.send({  data:user })
    }
        else{
            res.send("User Not Found");
        }
    }


app.post('/users',createuser);
app.get('/users',getusers);
app.put('/users/:user_id',UpdateData);
app.delete("/users/:user_id",delete_User);
app.get("/users/:user_id",getuser)
app.listen(5000,()=>{
    console.log("App Is running or port 5000");
})