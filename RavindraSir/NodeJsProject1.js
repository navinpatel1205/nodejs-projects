const express = require('express');

const app= express();
app.use(express.json()); //inbuild middleware


function printRoute(req,res,next)
{
    
    console.log(req.originalUrl);
    next();
}
app.use(printRoute);

function authenticate(req,res,next)//middleware
{
    const authentic=req.query.authentic;
    if(authentic==1){
        next();
    }
    else{
        res.status(404).send("Sorry !! You are not authenticated!!")
    }

}
const userBlog=[];


///Api for create blog
function createBlog(req,res)
{
    const id =req.body.Id;
    const title =req.body.title;
    const content =req.body.content;
    const public =req.body.public;
    const objData={id,title,content,public};
    userBlog.push(objData);
    res.send({ 
        msg:"New Blog added!!",
        data:objData
    })
}


//Api for get all blogs
function getBlog(req,res){
    const public=req.query.public;
    if(public){
        const publicview= userBlog.filter(elm=>elm.public ===true);
        res.send({data:publicview});
        return;
    }
    else
    {res.send({data:userBlog});}
}

//Api for Update Blog post
function updateBlog(req,res)
{
    bid=parseInt(req.params.bid); 
    const title =req.body.title;
    const content =req.body.content;
    const public =req.body.public;
    const idx=userBlog.findIndex(elm=>elm.id == bid);
    if(idx>=0)
    {   
        const updated_blog={
            id : bid,
            title ,
              content,
                public
        }
        userBlog[idx]=updated_blog;
        res.send({ 
            msg:"Blog Updated Success",
            data:updated_blog
        })

    }else{
        res.status(404).send("Blog Not Found");
    }
}


//Api for Delete any Blog
function deleteBlog(req,res)
{
    bid=parseInt(req.params.bid);
    const idx=userBlog.findIndex(elm=>elm.id == bid);
    if(idx>=0)
    {   
       
        userBlog.splice(idx,1);
        res.send({ 
            msg:"Blog Deleted Success"   })

    }else{
        res.status(404).send("Blog Not Found");
    }
}


//Api for get Blog using id
function getsingleBlog(req,res)
{
    bid=parseInt(req.params.bid);
    const idx=userBlog.find(elm=>elm.id == bid);
    if(idx)
    {
        res.send({idx})
    }
    else{
        res.status(404).send("Blog Not Found");
    }
}

app.post("/blogposts",createBlog);
app.get("/blogposts",getBlog)
app.put("/blogposts/:bid",updateBlog);
app.delete("/blogposts/:bid",deleteBlog);
app.get("/blog/:bid",authenticate,getsingleBlog);

app.listen(5000,()=>
{
    console.log("App is running on port 5000");
})