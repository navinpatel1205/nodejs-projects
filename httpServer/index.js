// The http.createServer() method includes request and response parameter whic s supplied by Node.js
//The request object can be used to get information about the curent http request
// eg URL,request, header and data.


const http = require('http');
const fs= require('fs');



const server= http.createServer((req,res)=>{
   const data= fs.readFileSync('./UserAPI/userAPI.json','UTF-8');
        const objdata=JSON.parse(data);
    // console.log(req.url);
    if(req.url=='/'){
        res.end('Hello from  Home page');
    }else 
    if(req.url=='/about'){
        res.end('Hello from  About page');
    }else if(req.url=='/contact'){
        res.end('Hello from contact page');
       }
    else if(req.url=='/userapi'){
        res.writeHead(200,{'content-type':'application/json'})
            res.end(objdata);
}else{
        res.writeHead(404,{"Content-type": "text/html"})
        res.end('<h1>Page Does Not exist</h1>');

    }
    
   

});

server.listen(8000,'127.0.0.1',() => {
    console.log("listening at port 8000");
});