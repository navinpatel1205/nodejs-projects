//Event module
//Node.js has a build in module, called "Events"
//where you can create-,fire-, and listen for- your own Events.
//Example 1- registering for the event to be fired only one time using once.

const EventsEmitter = require('events');

const event= new EventsEmitter() ;


event.on('sayMyName',()=>{
    console.log("your name is Navin");  
})
event.on('sayMyName',()=>{
    console.log("your age is 23");  
})
event.on('sayMyName',()=>{
    console.log("your love is xyz");  
})
//xample-2 create an events emitter instance and register  a couple of callbacks
event.emit("sayMyName");
//Example-3 registering for the event with calback parameters
event.on("checkpage",(sc,msg)=>{
console.log(`status code ${sc} and the page is ${msg}`)
})
event.emit("checkpage",200,"ok");



